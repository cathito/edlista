package ifs.edu.br.edwardlist;

import ifs.edu.br.edwardlist.Pessoa;
import ifs.edu.br.edwardlist.interfaces.ILista;
import ifs.edu.br.edwardlist.lista.dupla.ListaDE;
import ifs.edu.br.edwardlist.lista.dupla.circular.ListaDEC;
import ifs.edu.br.edwardlist.lista.simples.Lista;
import ifs.edu.br.edwardlist.lista.simples.cicular.ListaSEC;

public class MainLista {
    public static void main(String[] args) {
        ListaDEC lista = new ListaDEC();
        Pessoa p = new Pessoa("Edu1", 18);

        lista.add(new Pessoa("Maria", 12));
        lista.add(p);
        lista.add(new Pessoa("Ana", 15));
        lista.add(new Pessoa("Edu", 23));
        lista.addMeio(new Pessoa("Edu0", 69), 2);
//        lista.addFim(new Pessoa("Edu45", 16));
//        lista.addFim(new Pessoa("Aaanaaa", 1));
//        lista.addFim(new Pessoa("Zefinha", 78));
//        lista.del();
//        lista.delFim();
//        lista.delMeio(1);
//        lista.del();
//        lista.del();
        //lista.del();

        lista.listar();
        System.out.println("----------------- " + lista.size());
        ILista l = lista.ordenar(p.nome);
        l.listar();

    }
}
