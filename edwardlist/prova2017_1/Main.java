package ifs.edu.br.edwardlist.prova2017_1;

import ifs.edu.br.edwardlist.Pessoa;
import ifs.edu.br.edwardlist.interfaces.IPilha;
import ifs.edu.br.edwardlist.lista.simples.Lista;
import ifs.edu.br.edwardlist.no.No;
import ifs.edu.br.edwardlist.pilha.simples.Pilha;

public class Main {
    public static void main(String[] args) {
        Pilha pilhaA = new Pilha();
        pilhaA.push(new Item("A"));
        pilhaA.push(new Item("B"));

        No n = pilhaA.top();
//        while (n != null){
//            System.out.println(((Item)n.dado).valor);
//            n = n.proximo;
//        }

        Pilha pilhaB = new Pilha();
        pilhaB.push(new Item("A"));
        pilhaB.push(new Item("C"));

//        System.out.println("------------");
//
//        n = pilhaB.top();
//        while (n != null){
//            System.out.println(((Item)n.dado).valor);
//            n = n.proximo;
//        }
//
//        System.out.println("Dados da pliha A------------");

        Lista lista = new Algoritmo().preencheLista(pilhaA, pilhaB);

        System.out.println("Lista -------------");
        n = lista.primeiro;
        while (n != null){
            System.out.println(((Item)n.dado).valor);
            n = n.proximo;
        }

        System.out.println("Pilha A -----------");
        n = pilhaA.top();
        while (n != null){
            System.out.println(((Item)n.dado).valor);
            n = n.proximo;
        }
        System.out.println("Pilha B -----------");
        n = pilhaB.top();
        while (n != null){
            System.out.println(((Item)n.dado).valor);
            n = n.proximo;
        }
    }
}
