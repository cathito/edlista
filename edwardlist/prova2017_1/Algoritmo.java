package ifs.edu.br.edwardlist.prova2017_1;

import ifs.edu.br.edwardlist.interfaces.ILista;
import ifs.edu.br.edwardlist.interfaces.IPilha;
import ifs.edu.br.edwardlist.lista.simples.Lista;
import ifs.edu.br.edwardlist.no.No;
import ifs.edu.br.edwardlist.pilha.simples.Pilha;

public class Algoritmo
{
    private Lista preencheLista(Pilha pb, Pilha pa, Lista lista)
    {
        No noB = (No) pb.top();
        while (noB != null)
        {
            No noA = (No) pa.top();
            Item itemB = (Item) noB.dado;
            boolean exist = false;

            while (noA != null)
            {
                if (itemB.valor.compareTo(((Item)noA.dado).valor) == 0){ exist = true; break; }
                noA = noA.proximo;
            }

            if (!exist){ lista.addFim(itemB); }
            noB = noB.proximo;
        }
        return lista;
    }

    public Lista preencheLista(Pilha pa, Pilha pb)
    {
        Lista lista = new Lista();

        No noA = (No) pa.top();
        while (noA != null)
        {
            No noB = (No) pb.top();
            Item itemA = (Item) noA.dado;

            boolean exist = false;

            while (noB != null)
            {
                if (itemA.valor.compareTo(((Item)noB.dado).valor) == 0){ exist = true; break; }
                noB = noB.proximo;
            }

            if (!exist){ lista.addFim(itemA); }
            noA = noA.proximo;
        }
        return preencheLista(pb, pa, lista);
    }
}