package ifs.edu.br.edwardlist.prova2017_1;

import ifs.edu.br.edwardlist.interfaces.Itens;

public class Item extends Itens<Item> {
    public String valor;

    public Item(String s){
        this.valor = s;
    }

    @Override
    public int compareInt(Item var) {
        return 0;
    }

    @Override
    public int compareStr(Item var) {
        return this.valor.compareTo(var.valor);
    }
}
