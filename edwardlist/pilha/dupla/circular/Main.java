package ifs.edu.br.edwardlist.pilha.dupla.circular;

import ifs.edu.br.edwardlist.no.No;

public class Main {
    public static void main(String[] args) {
        PilhaDEC pilha = new PilhaDEC();
        pilha.push(new Pessoa(23));
        pilha.push(new Pessoa(12));
        pilha.push(new Pessoa(15));
        pilha.push(new Pessoa(9));
        pilha.pop();
        pilha.pop();
//        pilha.pop();
//        pilha.pop();
        System.out.println("Total: " + pilha.size());

        No atual = pilha.top();
        Pessoa p = new Pessoa();

        while (atual != null){
            p = (Pessoa) atual.dado;

            System.out.println(p.idade);
            if (atual.proximo.equals(pilha.top())){
                break;
            }else{
                atual = atual.proximo;
            }
        }
        System.out.println("--------------------------");
        while (atual != null){
            p = (Pessoa) atual.dado;

            System.out.println(p.idade);
            if (atual.equals(pilha.top())){
                break;
            }else{
                atual = atual.anterior;
            }
        }
    }
}
