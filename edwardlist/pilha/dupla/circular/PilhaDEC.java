package ifs.edu.br.edwardlist.pilha.dupla.circular;

import ifs.edu.br.edwardlist.interfaces.IPilha;
import ifs.edu.br.edwardlist.no.No;

public class PilhaDEC implements IPilha {
    public No primeiro;
    public int total = 0;


    @Override
    public boolean push(Object o) {
        No novo = new No();
        novo.dado = o;

        if (primeiro == null){
            primeiro = novo;
            primeiro.anterior = novo;
            primeiro.proximo = novo;
            total++;
        }else{
            No ultimo = primeiro.anterior;
            novo.proximo = primeiro;
            ultimo.proximo = novo;
            primeiro.anterior = novo;
            novo.anterior = ultimo;
            primeiro = novo;
            total++;
        }
        return true;
    }

    @Override
    public boolean pop() {
        if (size() > 1){
            No ultimo = primeiro.anterior;
            primeiro = primeiro.proximo;
            primeiro.anterior = ultimo;
            ultimo.proximo = primeiro;
        }else{
            primeiro = null;
        }
        total--;
        return true;
    }

    @Override
    public No top() { return this.primeiro; }

    @Override
    public int size() { return this.total; }
}
