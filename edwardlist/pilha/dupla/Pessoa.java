package ifs.edu.br.edwardlist.pilha.dupla;

public class Pessoa {
    public int idade;

    Pessoa(int idade){
        setIdade(idade);
    }

    public Pessoa() {

    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
}
