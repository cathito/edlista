package ifs.edu.br.edwardlist.pilha.dupla;

import ifs.edu.br.edwardlist.no.No;

public class Main {
    public static void main(String[] args) {
        PilhaDE pilha = new PilhaDE();
        pilha.push(new Pessoa(23));
        pilha.push(new Pessoa(12));
        pilha.push(new Pessoa(15));
        pilha.push(new Pessoa(9));
//        pilha.pop();
//        pilha.pop();
        System.out.println("Total: " + pilha.size());

        No atual = pilha.top();
        Pessoa p = new Pessoa();

        while (atual != null){
            p = (Pessoa) atual.dado;

            System.out.println(p.idade);
            if (atual.proximo == null){
                break;
            }else{
                atual = atual.proximo;
            }
        }
        System.out.println("--------------------------");
        while (atual != null){
            p = (Pessoa) atual.dado;

            System.out.println(p.idade);
            if (atual.anterior == null){
                break;
            }else{
                atual = atual.anterior;
            }
        }
    }
}
