package ifs.edu.br.edwardlist.pilha.dupla;

import ifs.edu.br.edwardlist.interfaces.IPilha;
import ifs.edu.br.edwardlist.no.No;

public class PilhaDE implements IPilha {
    public No primeiro;
    public int total = 0;


    @Override
    public boolean push(Object o) {
        No novo = new No();
        novo.dado = o;

        if (primeiro == null){
            primeiro = novo;
            total++;
        }else{
            primeiro.anterior = novo;
            novo.proximo = primeiro;
            primeiro = novo;
            total++;
        }
        return true;
    }

    @Override
    public boolean pop() {
        primeiro = primeiro.proximo;
        primeiro.anterior = null;
        total--;
        return true;
    }

    @Override
    public No top() { return this.primeiro; }

    @Override
    public int size() { return this.total; }
}
