package ifs.edu.br.edwardlist.pilha.simples;

import ifs.edu.br.edwardlist.interfaces.IPilha;
import ifs.edu.br.edwardlist.no.No;

public class Pilha implements IPilha {
    public No primeiro;
    public int total = 0;


    @Override
    public boolean push(Object o) {
        No novo = new No();
        novo.dado = o;

        if (primeiro == null){
            primeiro = novo;
            total++;
        }else{
            novo.proximo = primeiro;
            primeiro = novo;
            total++;
        }
        return true;
    }

    @Override
    public boolean pop() {
        primeiro = primeiro.proximo;
        total--;
        return true;
    }

    @Override
    public No top() { return this.primeiro; }

    @Override
    public int size() { return this.total; }
}
