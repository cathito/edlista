package ifs.edu.br.edwardlist.pilha.simples;

import ifs.edu.br.edwardlist.no.No;

public class Main {
    public static void main(String[] args) {
        Pilha pilha = new Pilha();
        pilha.push(new Pessoa(23));
        pilha.push(new Pessoa(12));
        pilha.push(new Pessoa(15));
        pilha.push(new Pessoa(9));
        pilha.pop();
        pilha.pop();
        System.out.println("Total: " + pilha.size());

        No atual = pilha.top();
        Pessoa p = new Pessoa();

        while (atual != null){
            p = (Pessoa) atual.dado;

            System.out.println(p.idade);
            atual = atual.proximo;
        }
    }
}
