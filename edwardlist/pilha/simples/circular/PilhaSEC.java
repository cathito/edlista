package ifs.edu.br.edwardlist.pilha.simples.circular;

import ifs.edu.br.edwardlist.interfaces.IPilha;
import ifs.edu.br.edwardlist.no.No;

public class PilhaSEC implements IPilha {
    public No primeiro;
    public No ultimo;
    public int total = 0;


    @Override
    public boolean push(Object o) {
        No novo = new No();
        novo.dado = o;

        if (primeiro == null){
            primeiro = novo;
            ultimo = novo;
            primeiro.proximo = primeiro;
            total++;
        }else{
            novo.proximo = primeiro;
            ultimo.proximo = novo;
            primeiro = novo;
            total++;
        }
        return true;
    }

    @Override
    public boolean pop() {
        if (size() > 1){
            primeiro = primeiro.proximo;
            ultimo.proximo = primeiro;
        }else{
            primeiro = null;
            ultimo = null;
        }
        total--;
        return true;
    }

    @Override
    public No top() { return this.primeiro; }

    @Override
    public int size() { return this.total; }
}
