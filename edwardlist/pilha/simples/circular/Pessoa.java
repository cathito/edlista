package ifs.edu.br.edwardlist.pilha.simples.circular;

public class Pessoa {
    public int idade;

    Pessoa(int idade){
        setIdade(idade);
    }

    public Pessoa() {

    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
}
