package ifs.edu.br.edwardlist.lista.simples.cicular;

import ifs.edu.br.edwardlist.interfaces.ILista;
import ifs.edu.br.edwardlist.no.No;
import ifs.edu.br.edwardlist.order.Ordenacao;
import ifs.edu.br.edwardlist.Pessoa;

public class ListaSEC extends Ordenacao implements ILista {
    public No primeiro;
    public No ultimo;
    public int total = 0;

    public ListaSEC(){}

    @Override
    public ILista ordenar(Object chave) {
        return ordenar(this, chave);
    }

    @Override
    public boolean add(Object o) {
        No novo = new No();
        novo.dado = o;

        if (primeiro == null){
            primeiro = novo;
            ultimo = novo;
            primeiro.proximo = novo;
            total++;
        }else{
            novo.proximo = primeiro;
            ultimo.proximo = novo;
            primeiro = novo;
            total++;
        }
        return true;
    }

    @Override
    public boolean addMeio(Object o, int posicao) {
        if (primeiro != null)
        {
            if (posicao < this.size()){
                No atual = new No();
                atual = primeiro;
                No novo = new No();
                novo.dado = o;

                if (posicao == 0) {
                    add(o);
                    return true;
                }else{
                    for (int i = 1; i < posicao; i++) {
                        atual = atual.proximo;
                    }
                    novo.proximo = atual.proximo;
                    atual.proximo = novo;
                    total++;
                    return true;
                }
            }else{
                System.out.println("Posição "+ posicao +" inválida. Ultima posição: " + (size()-1));
                return false;
            }
        }else{ this.add(o); return true; }
    }

    @Override
    public boolean addFim(Object o) {
        if (ultimo != null)
        {
            No novo = new No();
            novo.dado = o;

            ultimo.proximo = novo;
            novo.proximo = primeiro;
            ultimo = novo;
            total++;
            return true;
        }else{
            add(o);
            return true;
        }
    }

    @Override
    public boolean del() {
        if (size() > 1){
            primeiro = primeiro.proximo;
            ultimo.proximo = primeiro;
        }else{
            primeiro = null;
            ultimo = null;
        }
        total--;
        return true;
    }

    @Override
    public boolean delMeio(int posicao) {
        if (primeiro != null && posicao < size())
        {
            if (posicao == 0){
                del();
                return true;
            }else if (posicao == (size()-1)){
                delFim();
                return true;
            }else{
                No atual = new No();
                atual = primeiro;

                for (int i = 1; i < posicao; i++) {
                    atual = atual.proximo;
                }

                atual.proximo = atual.proximo.proximo;
                total--;
                return true;
            }
        }else{ return false; }
    }

    @Override
    public boolean delFim() {
        if (ultimo != null)
        {
            No atual = new No();
            atual = primeiro;

            if (size() > 1){
                while (atual.proximo.proximo != primeiro){
                    atual = atual.proximo;
                }

                atual.proximo = primeiro;
                ultimo = atual;
            }else{
                primeiro = null;
                ultimo = null;
            }
            total--;
            return true;
        }else{ return false; }
    }

    @Override
    public void listar() {
        No atual = new No();
        atual = primeiro;
        Pessoa p;

        while (atual != null){
            p = (Pessoa) atual.dado;
            System.out.print(p.nome + ", ");
            System.out.println(p.idade);
            if (atual.proximo.equals(primeiro)){ break; }
            atual = atual.proximo;
        }
    }

    @Override
    public int getIndex(Object o) {
        if (primeiro != null){
            No temp = new No();
            temp = primeiro;

            int i;
            for (i = 0; i < size(); i++) {
                if (temp.dado.equals(o)){
                    break;
                }else{
                    temp = temp.proximo;
                }
            }
            return i;
        }else{
            return 0;
        }
    }

    @Override
    public No getNo(int posicao) {
        if (primeiro != null){
            No temp = new No();
            temp = primeiro;

            for (int i = 0; i < posicao; i++) {
                temp = temp.proximo;
            }
            return temp;
        }else{
            return null;
        }
    }

    @Override
    public int size() { return this.total; }
}
