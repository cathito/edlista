package ifs.edu.br.edwardlist.lista.dupla;

import ifs.edu.br.edwardlist.interfaces.ILista;
import ifs.edu.br.edwardlist.no.No;
import ifs.edu.br.edwardlist.order.Ordenacao;
import ifs.edu.br.edwardlist.Pessoa;

public class ListaDE extends Ordenacao implements ILista {
    public No primeiro, ultimo;
    public int total = 0;

    @Override
    public ILista ordenar(Object chave) {
        return ordenar(this, chave);
    }

    @Override
    public boolean add(Object o) {
        No novo = new No();
        novo.dado = o;
        if (primeiro == null){
            primeiro = novo;
            ultimo = novo;
        }else{
            novo.proximo = primeiro;
            primeiro.anterior = novo;
            primeiro = novo;
        }
        total++;
        return true;
    }

    @Override
    public boolean addMeio(Object o, int posicao) {
        if (primeiro != null)
        {
            if (posicao < this.size()){
                if (posicao == 0){
                    add(o);
                    return true;
                }else{
                    No atual = new No();
                    No novo = new No();
                    novo.dado = o;

                    atual = primeiro;

                    for (int i = 1; i < posicao; i++) {
                        atual = atual.proximo;
                    }
                    novo.proximo = atual.proximo;
                    atual.proximo.anterior = novo;
                    atual.proximo = novo;
                    novo.anterior = atual;
                    total++;
                    return true;
                }
            }else{
                System.out.println("Posição "+ posicao +" inválida. Ultima posição: " + (this.size()-1));
                return false;
            }
        }else{ this.add(o); return true; }
    }

    @Override
    public boolean addFim(Object o) {
        if (ultimo != null){
            No novo = new No();
            novo.dado = o;
            ultimo.proximo = novo;
            novo.anterior = ultimo;
            ultimo = novo;
            total++;
            return true;
        }else{
            add(o);
            return true;
        }
    }

    @Override
    public boolean del() {
        if (size() > 0){
            if (size() > 1){
                primeiro.proximo.anterior = null;
                primeiro = primeiro.proximo;
                total--;
            }else{
                primeiro = null;
                ultimo = null;
                total--;
            }
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean delMeio(int posicao) {
        if (primeiro != null && posicao < this.size()){
            if (posicao == 0) {
                del();
                return true;
            }else if (posicao == (size()-1)){
                delFim();
                return true;
            }else{
                No atual = new No();
                atual = primeiro;

                for (int i = 1; i < posicao; i++) {
                    atual = atual.proximo;
                }
                    atual.proximo = atual.proximo.proximo;
                    atual.proximo.anterior = atual;
                    total--;
                    return true;
                }
        }else{
            return false;
        }
    }

    @Override
    public boolean delFim() {
        if (size() == 1){
            primeiro = null;
            ultimo = null;
            total--;
            return true;
        }else{
            ultimo = ultimo.anterior;
            ultimo.proximo = null;
            total--;
            return true;
        }
    }

    @Override
    public void listar() {
        No novo = new No();
        novo = primeiro;
        Pessoa p;
        while (novo != null){
            p = (Pessoa) novo.dado;
            System.out.print(p.nome + ", ");
            System.out.println(p.idade);
            novo = novo.proximo;
        }
    }

    @Override
    public int getIndex(Object o) {
        if (primeiro != null){
            No temp = new No();
            temp = primeiro;

            int i;
            for (i = 0; i < size(); i++) {
                if (temp.dado.equals(o)){
                    break;
                }else{
                    temp = temp.proximo;
                }
            }
            return i;
        }else{
            return 0;
        }
    }

    @Override
    public No getNo(int posicao) {
        if (primeiro != null){
            No temp = new No();
            temp = primeiro;

            for (int i = 0; i < posicao; i++) {
                temp = temp.proximo;
            }
            return temp;
        }else{
            return null;
        }
    }

    @Override
    public int size() { return this.total; }
}
