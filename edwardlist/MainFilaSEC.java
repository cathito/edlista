package ifs.edu.br.edwardlist;

import ifs.edu.br.edwardlist.fila.simples.circular.FilaSEC;
import ifs.edu.br.edwardlist.no.No;

public class MainFilaSEC {
    public static void main(String[] args) {
        FilaSEC fila = new FilaSEC();
        fila.push(new Pessoa(23));
        fila.push(new Pessoa(12));
        fila.push(new Pessoa(15));
        fila.push(new Pessoa(9));
        fila.pop();
        fila.pop();
//        fila.pop();
//        fila.pop();
        System.out.println("Total: " + fila.size());

        No atual = fila.top();
        Pessoa p = new Pessoa();

        for (int i = 0; i < fila.size(); i++) {
            p = (Pessoa) atual.dado;
            System.out.println(p.idade);
            atual = atual.proximo;
        }
    }
}
