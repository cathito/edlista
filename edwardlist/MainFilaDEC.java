package ifs.edu.br.edwardlist;

import ifs.edu.br.edwardlist.fila.dupla.circular.FilaDEC;
import ifs.edu.br.edwardlist.no.No;

public class MainFilaDEC {
    public static void main(String[] args) {
        FilaDEC fila = new FilaDEC();
        fila.push(new Pessoa(23));
        fila.push(new Pessoa(12));
        fila.push(new Pessoa(15));
        fila.push(new Pessoa(9));
        fila.pop();
        fila.pop();
//        fila.pop();
//        fila.pop();
        System.out.println("Total: " + fila.size());

        No atual = fila.top();
        Pessoa p = new Pessoa();

        while (atual != null){
            p = (Pessoa) atual.dado;

            System.out.println(p.idade);
            if (atual.proximo.equals(fila.top())){
                break;
            }else{
                atual = atual.proximo;
            }
        }

        System.out.println("-------------------");
        while (atual != null){
            p = (Pessoa) atual.dado;

            System.out.println(p.idade);
            if (atual.equals(fila.top())){
                break;
            }else{
                atual = atual.anterior;
            }
        }
    }
}
