package ifs.edu.br.edwardlist.interfaces;

public interface IPilha
{
    boolean push(Object o);
    boolean pop();
    Object top();
    int size();
}