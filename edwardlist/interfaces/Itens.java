package ifs.edu.br.edwardlist.interfaces;

public abstract class Itens<T> {

    public abstract int compareInt(T var);
    public abstract int compareStr(T var);

    public int compareTo(T var, Object chave)
    {
        if (chave.getClass().equals(new String().getClass())){
            return compareStr(var);
        }else{
            return compareInt(var);
        }
    }
}
