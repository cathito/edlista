package ifs.edu.br.edwardlist.interfaces;

import ifs.edu.br.edwardlist.no.No;

public interface ILista
{
    boolean add(Object o);
    boolean addMeio(Object o, int posicao);
    boolean addFim(Object o);
    boolean del();
    boolean delMeio(int posicao);
    boolean delFim();
    void listar();
    int getIndex(Object o);
    No getNo(int posicao);
    int size();
}