package ifs.edu.br.edwardlist.interfaces;

public interface IFila
{
    boolean push(Object o);
    boolean pop();
    Object top();
    int size();
}