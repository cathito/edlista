package ifs.edu.br.edwardlist;

import ifs.edu.br.edwardlist.fila.simples.Fila;
import ifs.edu.br.edwardlist.no.No;

public class MainFila {
    public static void main(String[] args) {
        Fila fila = new Fila();
        fila.push(new Pessoa("Ana", 23));
        fila.push(new Pessoa("jose", 12));
        fila.push(new Pessoa("Vrenorica", 15));
        fila.push(new Pessoa("Renata", 9));
        fila.pop();
//        fila.pop();
//        fila.pop();
//        fila.pop();
        System.out.println("Total: " + fila.size());

        No atual = fila.top();
        Pessoa p = new Pessoa();

        while (atual != null){
            p = (Pessoa) atual.dado;

            System.out.println(p.idade);
            atual = atual.proximo;
        }
    }
}
