package ifs.edu.br.edwardlist;

import ifs.edu.br.edwardlist.interfaces.Itens;

public class Pessoa extends Itens<Pessoa> {
    public int idade;
    public String nome;

    public Pessoa(){}

    public Pessoa(int i){
        this.idade = i;
    }

    public Pessoa(String n, int i){
        this.idade = i;
        this.nome = n;
    }

    @Override
    public int compareInt(Pessoa var) {
        if (this.idade > var.idade){
            return 1;
        }else if (this.idade < var.idade){
            return -1;
        }
        return 0;
    }

    @Override
    public int compareStr(Pessoa var) {
        return this.nome.compareTo(var.nome);
    }
}
