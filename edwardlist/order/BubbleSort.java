package ifs.edu.br.edwardlist.order;

import ifs.edu.br.edwardlist.interfaces.ILista;
import ifs.edu.br.edwardlist.interfaces.Itens;
import ifs.edu.br.edwardlist.no.No;

public class BubbleSort {

    public ILista bubbleSort(ILista lista, Object chave)
    {
        for (int i = lista.size()-1; i >= 1 ; i--)
        {
            for (int j = 0; j < i; j++)
            {
                No n1 = lista.getNo(j);
                No n2 = lista.getNo(j+1);

                if (((Itens) n1.dado).compareTo((Itens) n2.dado, chave) < 0){
                    Itens temp = (Itens) n1.dado;
                    n1.dado = n2.dado;
                    n2.dado = temp;
                }
            }
        }
        return lista;
    }
}
