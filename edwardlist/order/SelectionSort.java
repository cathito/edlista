package ifs.edu.br.edwardlist.order;

import ifs.edu.br.edwardlist.interfaces.ILista;
import ifs.edu.br.edwardlist.interfaces.Itens;
import ifs.edu.br.edwardlist.no.No;

public final class SelectionSort
{
    public ILista selectionSort(ILista lista, Object chave)
    {
        for(int i = 0; i < lista.size()-1; i++)
        {
            int min = i;
            for(int j = i+1; j < lista.size(); j++){
                No n1 = lista.getNo(min);
                No n2 = lista.getNo(j);

                if (((Itens) n2.dado).compareTo(((Itens) n1.dado), chave) < 0){
                    Itens aux = (Itens) n2.dado;
                    n2.dado = n1.dado;
                    n1.dado = aux;
                }
            }
        }
        return lista;
    }
}
