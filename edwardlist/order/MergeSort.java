package ifs.edu.br.edwardlist.order;

import ifs.edu.br.edwardlist.interfaces.ILista;
import ifs.edu.br.edwardlist.interfaces.Itens;
import ifs.edu.br.edwardlist.lista.simples.Lista;

public class MergeSort {

    public ILista mergeSort(ILista lista, Object chave){ return sort(lista, chave); }

    private ILista sort(ILista lista, Object chave)
    {
        if(lista.size() == 1) {
            return lista;
        }
        int pivo = lista.size()/2;
        ILista aux = new Lista();
        ILista esq = new Lista();
        ILista dir = new Lista();

        for (int i = 0; i < lista.size(); i++) {
            if (i<pivo) {
                esq.addFim(lista.getNo(i).dado);
            }else {
                dir.addFim(lista.getNo(i).dado);
            }
        }

        esq = (ILista) sort(esq, chave);
        dir = (ILista) sort(dir, chave);

        return merge(esq, dir, chave);

    }

    private ILista merge(ILista v1, ILista v2, Object chave)
    {
        ILista aux = new Lista();

        int posA = 0, posB = 0, posAux = 0;

        while(posA < v1.size() && posB < v2.size()){
            if (((Itens) v1.getNo(posA).dado).compareTo((Itens) v2.getNo(posB).dado, chave) <= 0){
                aux.addFim(v1.getNo(posA).dado);
                posA++;
            }else{
                aux.addFim(v2.getNo(posB).dado);
                posB++;
            }
            posAux++;
        }

        while(posA < v1.size()){
            aux.addFim(v1.getNo(posA).dado);
            posAux++;
            posA++;
        }

        while(posB < v2.size()){
            aux.addFim(v2.getNo(posB).dado);
            posAux++;
            posB++;
        }
        return aux;
    }
//======================================================================================//
}
