package ifs.edu.br.edwardlist.order;

import ifs.edu.br.edwardlist.interfaces.ILista;

public abstract class Ordenacao
{
    public abstract ILista ordenar(Object chave);

    public ILista ordenar(ILista lista, Object chave)
    {
        if (lista.size() > 0 && lista.size() <= 2)
        {
            System.out.println("Selection Sort");
            return new SelectionSort().selectionSort(lista, chave);
        }
        else if (lista.size() > 2 && lista.size() <= 4)
        {
            System.out.println("Bubble Sort");
            return new BubbleSort().bubbleSort(lista, chave);
        }
        else if (lista.size() > 4 && lista.size() <= 6)
        {
            System.out.println("Merge Sort");
            return new MergeSort().mergeSort(lista, chave);
        }
        else if (lista.size() > 6)
        {
            System.out.println("Quick Sort");
            return new QuickSort().quickSort(lista, chave);
        }else {
            System.out.println("Erro ao ordena");
            return null;
        }
    }

    public ILista selectionSort(ILista lista, Object chave){ return new SelectionSort().selectionSort(lista, chave); }

    public ILista bubbleSort(ILista lista, Object chave){
        return new BubbleSort().bubbleSort(lista, chave);
    }

    public ILista mergeSort(ILista lista, Object chave){
        return new MergeSort().mergeSort(lista, chave);
    }

    public ILista quickSort(ILista lista, Object chave){
        return new QuickSort().quickSort(lista, chave);
    }
}