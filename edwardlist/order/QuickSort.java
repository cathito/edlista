package ifs.edu.br.edwardlist.order;

import ifs.edu.br.edwardlist.interfaces.ILista;
import ifs.edu.br.edwardlist.interfaces.Itens;
import ifs.edu.br.edwardlist.no.No;

public class QuickSort {

    public ILista quickSort(ILista lista, Object chave){
        return ordenar(lista, 0, lista.size()-1, chave);
    }

    private int dividir(ILista v, int esq, int dir, Object chave){
        Itens pivo = (Itens) v.getNo(esq).dado;
        int i = esq+1, f = dir;

        while (i <= f){
            No n1 = v.getNo(i);
            No n2 = v.getNo(f);

            if (((Itens) n1.dado).compareTo(pivo, chave) <= 0){ i++; }
            else if (((Itens) n2.dado).compareTo(pivo, chave) > 0){ f--; }
            else{
                Itens aux = (Itens) n1.dado;
                n1.dado = n2.dado;
                n2.dado = aux;
                i++;
                f--;
            }
        }
        No temp = v.getNo(f);
        v.getNo(esq).dado = temp.dado;
        temp.dado = pivo;
        return f;
    }

    private ILista ordenar(ILista v, int inicio, int fim, Object chave){
        if (inicio < fim){
            int posicaopivo = dividir(v, inicio, fim, chave);
            ordenar(v, inicio, posicaopivo-1, chave);
            ordenar(v, posicaopivo+1, fim, chave);
        }else{
            return v;
        }
        return v;
    }
}
