package ifs.edu.br.edwardlist.fila.simples.circular;

import ifs.edu.br.edwardlist.interfaces.IFila;
import ifs.edu.br.edwardlist.no.No;

public class FilaSEC implements IFila {
    public No primeiro;
    public int total = 0;


    @Override
    public boolean push(Object o) {
        No novo = new No();
        novo.dado = o;

        if (primeiro == null){
            primeiro = novo;
            primeiro.proximo = primeiro;
            total++;
        }else{
            No atual = new No();
            atual = primeiro;
            while (!atual.proximo.equals(primeiro)){
                atual = atual.proximo;
            }
            atual.proximo = novo;
            novo.proximo = primeiro;
            total++;
        }
        return true;
    }

    @Override
    public boolean pop() {
        if (size() > 1){
            primeiro = primeiro.proximo;
        }else{
            primeiro = null;
        }
        total--;
        return true;
    }

    @Override
    public No top() { return this.primeiro; }

    @Override
    public int size() { return this.total; }
}
