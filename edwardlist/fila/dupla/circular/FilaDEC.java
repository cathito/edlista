package ifs.edu.br.edwardlist.fila.dupla.circular;

import ifs.edu.br.edwardlist.interfaces.IFila;
import ifs.edu.br.edwardlist.no.No;

public class FilaDEC implements IFila {
    public No primeiro;
    public int total = 0;


    @Override
    public boolean push(Object o) {
        No novo = new No();
        novo.dado = o;

        if (primeiro == null){
            primeiro = novo;
            primeiro.anterior = primeiro;
            primeiro.proximo = primeiro;
            total++;
        }else{
            No atual = new No();
            atual = primeiro;
            while (!atual.proximo.equals(primeiro)){
                atual = atual.proximo;
            }
            atual.proximo = novo;
            novo.proximo = primeiro;
            novo.anterior = atual;
            primeiro.anterior = novo;
            total++;
        }
        return true;
    }

    @Override
    public boolean pop() {
        if (size() > 1){
            primeiro.anterior.proximo = primeiro.proximo;
            primeiro.proximo.anterior = primeiro.anterior;
            primeiro = primeiro.proximo;
        }else{
            primeiro = null;
        }
        total--;
        return true;
    }

    @Override
    public No top() { return this.primeiro; }

    @Override
    public int size() { return this.total; }
}
