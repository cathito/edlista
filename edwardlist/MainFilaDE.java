package ifs.edu.br.edwardlist;

import ifs.edu.br.edwardlist.fila.dupla.FilaDE;
import ifs.edu.br.edwardlist.no.No;

public class MainFilaDE {
    public static void main(String[] args) {
        FilaDE filaDE = new FilaDE();
        filaDE.push(new Pessoa(23));
        filaDE.push(new Pessoa(12));
        filaDE.push(new Pessoa(15));
        filaDE.push(new Pessoa(9));
        filaDE.pop();
        filaDE.pop();
//        filaDE.pop();
//        filaDE.pop();
        System.out.println("Total: " + filaDE.size());

        No atual = filaDE.top();
        Pessoa p = new Pessoa();

        while (atual != null){
            p = (Pessoa) atual.dado;

            System.out.println(p.idade);
            if (atual.proximo == null){
                break;
            }else{
                atual = atual.proximo;
            }
        }

        System.out.println("-------------------");
        while (atual != null){
            p = (Pessoa) atual.dado;

            System.out.println(p.idade);
            atual = atual.anterior;
        }
    }
}
